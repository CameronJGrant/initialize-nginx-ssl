#!/bin/bash
BASEDIR=$(dirname "$0")
# This is a sample script for backing up your containers. Set to a cronjob.
# Example crontab
# 0 23 * * * /bin/sh $BASEDIR/scripts/backup_wp.sh

DOCKER="/usr/bin/docker"

cd "$BASEDIR"
rm -rf db.tar  letsencrypt.tar  wordpress.tar wiki-db.tar
$DOCKER run --rm --volumes-from wordpress -v $(pwd):/backup ubuntu tar cvf /backup/wordpress.tar /var/www/html
$DOCKER run --rm --volumes-from db -v $(pwd):/backup ubuntu tar cvf /backup/db.tar /var/lib/mysql
$DOCKER run --rm --volumes-from webserver -v $(pwd):/backup ubuntu tar cvf /backup/letsencrypt.tar /etc/letsencrypt
$DOCKER run --rm --volumes-from wiki-db -v $(pwd):/backup ubuntu tar cvf /backup/wiki-db.tar /var/lib/postgresql/data