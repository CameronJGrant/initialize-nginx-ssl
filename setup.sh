#!/bin/bash
COMPOSE="/usr/bin/docker-compose --no-ansi"
DOCKER="/usr/bin/docker"
BASEDIR=$(dirname "$0")

echo "Enter your email address"
read EMAIL
export EMAIL
echo "Enter your cert name (Can be anything)"
read CERTNAME
export CERTNAME
echo "Enter your domain names. Press enter after each name. Press enter again to finish."
read DOMAIN
while [ -n "$DOMAIN" ]
do
  DOMAINS="$DOMAINS -d $DOMAIN"
  read DOMAIN
done
export DOMAINS
export COMMAND="--cert-name $CERTNAME"



envsubst < setup/docker-compose.yml > docker-compose-setup.yml

mkdir nginx-conf

envsubst < setup/nginx-conf-setup/nginx.conf > nginx-conf/nginx.conf

$COMPOSE -f docker-compose-setup.yml up --abort-on-container-exit

$DOCKER system prune -af

rm docker-compose-setup.yml
rm nginx-conf/nginx.conf

export COMMAND=--no-eff-email --force-renewal

cp setup/nginx-conf/options-ssl-nginx.conf nginx-conf/options-ssl-nginx.conf
envsubst < setup/nginx-conf/nginx.conf > nginx-conf/nginx.conf
envsubst < setup/docker-compose.yml > docker-compose.yml

(crontab -l ; echo "0 12 * * * $BASEDIR/scripts/ssl_renew.sh >> /var/log/cron.log 2>&1")| crontab -

$COMPOSE up -d
